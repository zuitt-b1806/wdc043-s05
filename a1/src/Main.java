public class Main {
    public static void main(String[] args) {

        /*
        * Create a Java Class named uer.java with instance variable/proprties for firstName lastName age and address
        *
        *create a java class named course.java with intance variable/proprties for name, description seatts(int) fee(double) startdate end date and instructor(User)
        * */

        User user1 = new User("Lorren", "Ipsum", 30, "Kansas State City");

        Course course1 = new Course();

        course1.setName("Java 101");
        course1.setDescription("Basic Intro to Java");
        course1.setSeats(25);
        course1.setFee(15000.76);
        course1.setStartDate("Aug. 24, 2025");
        course1.setEndDate("Sept. 24, 2025");
        course1.setInstructor(user1);

        System.out.println("User's First Name: " + user1.getFirstName());
        System.out.println("User's Last Name: " + user1.getLastName());
        System.out.println("User's Age: " + user1.getAge());
        System.out.println("User's Address: " + user1.getAddress());
        System.out.println("Course name: " + course1.getName());
        System.out.println("Course description: " + course1.getDescription());
        System.out.println("Course seats: " + course1.getSeats());
        System.out.println("Course Instructor's name : " + course1.getInstructorName());
    }
}